#!/bin/bash

battery=`upower --enumerate | grep BAT`

get_vpn() {
    nmcli connection show --active | grep VPN > /dev/null
    [ $? = 0 ] && printf "VPN "
}

get_battery()
{
upower --show-info ${battery} | awk '
    BEGIN { THRESHOLD="15%"; }
    /state/{
        if ($2 == "charging")
        { state = "+" }
        else if ($2 == "discharging")
        { state = "-" }
        else
        { state = "0" }
     };
    /percentage/ {
        percent = $2;
    };
    /time to/{
        if ($5 == "minutes")
        { hours = 0; minutes = int($4); }
        else if ($5 == "hours")
        { hours = int( $4 );
          minutes = int(($4 - hours) * 60); }
    };
    END {
    if (state != "0") {
        if ( (percent < THRESHOLD) &&
             (state == "-") )
        { warn="WARNING: "; }
        printf( "%s%s (%c%02d:%02d)" ,
        warn, percent, state, hours, minutes); }
    }'
}

get_mpd()
{
    mpc --host=/home/joacar/.cache/mpd/socket | awk '
BEGIN { RS="\n\n"; FS="\n"; }
{
    status = ($2 ~ /^\[playing\]/);
    if (status)
    { printf( "[ %s ]" ,  $1); }
    else
    { printf(""); }
}'
}

get_disk()
{
    df -h / | awk '
BEGIN { THRESHOLD="85%"; }
($6 == "/"){
    if ( $5 > THRESHOLD )
    { printf("[ROOT %s]",$5); }
}
($6 == "/home"){
    if ( $5 > THRESHOLD )
    { printf("[HOME %s]",$5); }
}'
}

while true; do
    sleep 1
    get_battery
    get_mpd
    get_disk
    get_vpn
    echo
done
