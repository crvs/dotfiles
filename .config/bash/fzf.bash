# Setup fzf
# ---------
if ( [[ ! "$PATH" == *${HOME}/MyUtils/fzf/bin* ]] && [ -d "${HOME}/MyUtils/fzf/bin" ] ); then
  export PATH="${PATH:+${PATH}:}${HOME}/MyUtils/fzf/bin"
fi

# Auto-completion
# ---------------
( [[ $- == *i* ]] && [ -d "${HOME}/MyUtils/fzf/" ] ) && source "${HOME}/MyUtils/fzf/shell/completion.bash" 2> /dev/null
( [[ $- == *i* ]] && [ -d "/usr/share/fzf" ] ) && source "/usr/share/fzf/completion.bash" 2> /dev/null

# Key bindings
# ------------
localkb="${HOME}/MyUtils/fzf/shell/key-bindings.bash"
globalkb="/usr/share/fzf/key-bindings.bash"

if [ -f "${localkb}" ]; then
   source ${localkb}
elif [ -f "${globalkb}" ]; then
   source ${globalkb}
fi
